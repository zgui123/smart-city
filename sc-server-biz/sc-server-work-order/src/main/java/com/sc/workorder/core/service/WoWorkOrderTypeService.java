package com.sc.workorder.core.service;

import com.sc.common.service.BaseService;
import com.sc.workorder.entity.WoWorkOrderType;

/**
 * @author: wust
 * @date: 2020-04-01 10:13:20
 * @description:
 */
public interface WoWorkOrderTypeService extends BaseService<WoWorkOrderType>{
}
