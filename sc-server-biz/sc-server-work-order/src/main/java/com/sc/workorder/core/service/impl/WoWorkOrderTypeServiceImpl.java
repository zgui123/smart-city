package com.sc.workorder.core.service.impl;

import com.sc.workorder.core.dao.WoWorkOrderTypeMapper;
import com.sc.workorder.core.service.WoWorkOrderTypeService;
import com.sc.workorder.entity.WoWorkOrderType;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.sc.common.dto.WebResponseDto;

/**
 * @author: wust
 * @date: 2020-04-01 10:13:20
 * @description:
 */
@Service("woWorkOrderTypeServiceImpl")
public class WoWorkOrderTypeServiceImpl extends BaseServiceImpl<WoWorkOrderType> implements WoWorkOrderTypeService {
    @Autowired
    private WoWorkOrderTypeMapper woWorkOrderTypeMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return woWorkOrderTypeMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
