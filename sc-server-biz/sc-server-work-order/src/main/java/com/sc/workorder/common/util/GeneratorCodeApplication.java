package com.sc.workorder.common.util;


import com.sc.generatorcode.invoker.SingleInvoker;
import com.sc.generatorcode.invoker.base.Invoker;

/**
 * 生成代码入口
 */
public class GeneratorCodeApplication {

    public static void main(String[] args) {
        Invoker invoker = new SingleInvoker.Builder()
                .setTableName("wo_work_order_timeline")
                .setClassName("WoWorkOrderTimeline")
                .build();
        invoker.execute();
    }
}
