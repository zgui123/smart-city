/**
 * Created by wust on 2019-11-05 10:34:41
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.app.customer;


import com.sc.common.annotations.AppApi;
import com.sc.common.util.cache.SpringRedisTools;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: wust
 * @date: Created in 2019-11-05 10:34:41
 * @description: app客户登出
 *
 */
@Api(tags = {"app接口~客户登出"})
@AppApi
@RequestMapping("/api/app/v1/CustomerLogoutAppApi")
@RestController
public class CustomerLogoutAppApi {
    @Autowired
    private SpringRedisTools springRedisTools;

}
