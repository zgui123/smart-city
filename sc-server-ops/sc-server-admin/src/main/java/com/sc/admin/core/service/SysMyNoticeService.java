package com.sc.admin.core.service;

import com.sc.common.entity.admin.mynotice.SysMyNotice;
import com.sc.common.service.BaseService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SysMyNoticeService extends BaseService<SysMyNotice> {
    List<Map> getUnreadNoticeCount(@Param("status") String status, @Param("receiver") String receiver);
}
