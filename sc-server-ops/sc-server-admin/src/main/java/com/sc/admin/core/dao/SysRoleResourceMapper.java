package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.role.resource.SysRoleResource;
import org.springframework.dao.DataAccessException;



public interface SysRoleResourceMapper   extends IBaseMapper<SysRoleResource> {

    int deleteDirtyMenu()throws DataAccessException;

	int deleteDirtyResource()throws DataAccessException;
}
