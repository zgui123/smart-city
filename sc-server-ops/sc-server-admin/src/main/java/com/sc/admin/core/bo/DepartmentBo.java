package com.sc.admin.core.bo;

import cn.hutool.core.collection.CollectionUtil;
import com.sc.admin.core.dao.SysDepartmentMapper;
import com.sc.common.entity.admin.department.SysDepartment;
import com.sc.common.exception.BusinessException;
import com.sc.common.util.CodeGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class DepartmentBo {
    @Autowired
    private SysDepartmentMapper sysDepartmentMapper;

    /**
     * 根据部门code判断部门是否已经存在
     * @param code
     * @return
     */
    public boolean isExistsByCode(String code){
        SysDepartment departmentSearch = new SysDepartment();
        departmentSearch.setCode(code);
        List<SysDepartment> departments = sysDepartmentMapper.select(departmentSearch);
        if(CollectionUtil.isNotEmpty(departments)){
            return true;
        }
        return false;
    }


    public String getCode(){
        long start = System.currentTimeMillis();
        String code = CodeGenerator.genDetartmentCode();
        while (isExistsByCode(code)){
            code = CodeGenerator.genDetartmentCode();
            long end = System.currentTimeMillis();
            if((end - start) / 1000 > 20){ // 20秒超时设置，防止活锁
                throw new BusinessException("生成部门编码失败");
            }
        }
        return code;
    }
}
