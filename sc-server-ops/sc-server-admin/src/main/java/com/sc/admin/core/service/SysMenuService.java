package com.sc.admin.core.service;

import com.sc.common.entity.admin.menu.SysMenu;
import com.sc.common.service.BaseService;
import java.util.List;

/**
 * Created by wust on 2019/4/29.
 */
public interface SysMenuService extends BaseService<SysMenu> {

    /**
     * 平台超级管理员：非白名单大于1级的左侧菜单
     * @param type
     * @param lang 语言
     * @return
     */
    List<SysMenu> findAsideMenu4superAdmin(String type,String lang);


    /**
     * 平台普通管理员：非白名单大于1级的左侧菜单
     * @param permissionType
     * @param lang
     * @param accountId
     * @return
     */
    List<SysMenu> findAsideMenu4admin(String permissionType,String lang,Long accountId);

    /**
     * 员工：非白名单大于1级的左侧菜单
     * @param type
     * @param userId 登录的用户id
     * @param lang 语言
     * @return
     */
    List<SysMenu> findAsideMenuByUserId(String type,String lang,Long userId);



    /**
     * 平台超级管理员：非白名单等于1级的左侧菜单
     * @param permissionType 权限类型
     * @param lang 语言
     * @return
     */
    List<SysMenu> findOneLevelMenus4superAdmin(String permissionType,String lang);


    /**
     * 平台普通管理员：非白名单等于1级的左侧菜单
     * @param permissionType
     * @param lang
     * @param accountId
     * @return
     */
    List<SysMenu> findOneLevelMenus4admin(String permissionType,String lang, Long accountId);

    /**
     * 员工：非白名单等于1级的左侧菜单
     * @param permissionType 权限类型
     * @param userId 登录的用户id
     * @param lang 语言
     * @return
     */
    List<SysMenu> findOneLevelMenusByUserId(String permissionType,String lang,Long userId);



    List<SysMenu> findMenuNameByPermissionType(String type,String lang);
}
