package com.sc.admin.core.dao;

import com.sc.admin.entity.tenant.SysTenantAuthentication;
import com.sc.common.mapper.IBaseMapper;

/**
 * @author: wust
 * @date: 2020-12-03 10:21:44
 * @description:
 */
public interface SysTenantAuthenticationMapper extends IBaseMapper<SysTenantAuthentication>{
}