/**
 * Created by wust on 2020-03-30 10:44:32
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.cache;


import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.dao.SysDataSourceDetailMapper;
import com.sc.admin.core.dao.SysDataSourceMapper;
import com.sc.common.annotations.EnableComplexCaching;
import com.sc.common.cache.CacheAbstract;
import com.sc.common.entity.admin.datasource.SysDataSource;
import com.sc.common.entity.admin.datasource.detail.SysDataSourceDetail;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.*;

/**
 * @author: wust
 * @date: Created in 2020-03-30 10:44:32
 * @description: 数据源缓存
 *
 */
@EnableComplexCaching(dependsOnPOSimpleName = "SysDataSourceDetail")
@Component
public class RedisCacheDataSourceDetailBo extends CacheAbstract {
    @Autowired
    private SpringRedisTools springRedisTools;

    @Autowired
    private SysDataSourceMapper sysDataSourceMapper;

    @Autowired
    private SysDataSourceDetailMapper sysDataSourceDetailMapper;

    @Override
    public void init() {
        Set<String> keys2 = springRedisTools.keys(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_DETAIL_BY_DATA_SOURCE_ID.getStringValue().replaceAll("%s_","*"));
        if(keys2 != null && keys2.size() > 0){
            springRedisTools.deleteByKey(keys2);
        }

        cacheByDsId();
    }

    @Override
    public void reset() {

    }

    @Override
    public void add(Object obj){
        if(obj == null){
            return;
        }

        SysDataSourceDetail entity = null;

        if(obj instanceof Long){
            entity = sysDataSourceDetailMapper.selectByPrimaryKey(obj);
        }else if(obj instanceof SysDataSource){
            entity = (SysDataSourceDetail)obj;
        }else if(obj instanceof Map){
            entity = JSONObject.parseObject(JSONObject.toJSONString(obj),SysDataSourceDetail.class);
        }

        if(entity == null){
            return;
        }

        cacheByDsId(entity.getDataSourceId());
    }

    @Override
    public void batchAdd(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                add(o);
            }
        }
    }

    @Override
    public void updateByPrimaryKey(Object primaryKey){
        if(primaryKey == null){
            return;
        }

        SysDataSourceDetail entity = sysDataSourceDetailMapper.selectByPrimaryKey(primaryKey);

        cacheByDsId(entity.getDataSourceId());
    }


    @Override
    public void batchUpdate(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                updateByPrimaryKey(o);
            }
        }
    }

    @Override
    public void deleteByPrimaryKey(Object primaryKey){
        SysDataSourceDetail dataSource = sysDataSourceDetailMapper.selectByPrimaryKey(primaryKey);
        if(dataSource != null){
            String key3 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_DETAIL_BY_DATA_SOURCE_ID.getStringValue(),dataSource.getDataSourceId());
            if(springRedisTools.hasKey(key3)){
                springRedisTools.deleteByKey(key3);
            }
        }
    }

    @Override
    public void batchDelete(List<Object> primaryKeys){
        if(CollectionUtil.isNotEmpty(primaryKeys)){
            for (Object primaryKey : primaryKeys) {
                deleteByPrimaryKey(primaryKey);
            }
        }
    }




    private void cacheByDsId(){
        List<SysDataSource> dataSources = sysDataSourceMapper.selectAll();
        if(CollectionUtil.isNotEmpty(dataSources)){
            for (SysDataSource dataSource : dataSources) {
                cacheByDsId(dataSource.getId());
            }
        }
    }

    private void cacheByDsId(Long dsId){
        SysDataSourceDetail dataSourceDetailSearch = new SysDataSourceDetail();
        dataSourceDetailSearch.setDataSourceId(dsId);
        List<SysDataSourceDetail> dataSourceDetails = sysDataSourceDetailMapper.select(dataSourceDetailSearch);
        if(CollectionUtil.isNotEmpty(dataSourceDetails)){
            Map<Long,SysDataSourceDetail> groupMap = new HashMap();
            for (SysDataSourceDetail dataSourceDetail : dataSourceDetails) {
                Long key = dataSourceDetail.getId();
                groupMap.put(key,dataSourceDetail);
            }


            String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_DETAIL_BY_DATA_SOURCE_ID.getStringValue(), dsId);
            if(springRedisTools.hasKey(key2)){
                springRedisTools.deleteByKey(key2);
            }
            springRedisTools.addMap(key2,groupMap);
        }
    }
}
