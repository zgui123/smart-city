/**
 * Created by wust on 2019-10-22 15:13:39
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.mq.consumer;

import com.alibaba.fastjson.JSONObject;
import com.sc.common.util.MyStringUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author: wust
 * @date: Created in 2019-10-22 15:13:39
 * @description: 发布通知，该消费队列根据通知需要发送的渠道进行推送消息。
 *
 */
@Component
@RabbitListener(
        containerFactory = "singleListenerContainer",
        bindings = @QueueBinding(
                value = @Queue(
                        value = "${spring.rabbitmq.publishnotification.queue.name}",
                        durable = "${spring.rabbitmq.publishnotification.queue.durable}"
                ),
                exchange = @Exchange(
                        value = "${spring.rabbitmq.publishnotification.exchange.name}",
                        durable = "${spring.rabbitmq.publishnotification.exchange.durable}",
                        type = "${spring.rabbitmq.publishnotification.exchange.type}",
                        ignoreDeclarationExceptions = "${spring.rabbitmq.publishnotification.exchange.ignoreDeclarationExceptions}"
                ),
                key = "${spring.rabbitmq.publishnotification.routing-key}"
        )
)
public class Consumer4PublishNotificationQueue {
    static Logger logger = LogManager.getLogger(Consumer4PublishNotificationQueue.class);

    @RabbitHandler
    public void process(JSONObject jsonObject) {
        logger.info("消费到了，{}",jsonObject);

        String sendChannel = MyStringUtils.null2String(jsonObject.getString("sendChannel"));
        if(MyStringUtils.isBlank(sendChannel)){
            return;
        }
        String[] sendChannels = sendChannel.split(",");
        for (String channel : sendChannels) {
          // 此处获取到发送渠道后，进行推送消息
        }
    }
}
