package com.sc.common.exception;

import com.sc.common.dto.WebResponseDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BaseWebExceptionHandler {
    private static Log logger = LogFactory.getLog(BaseWebExceptionHandler.class);

    public WebResponseDto handle(Throwable e){
        WebResponseDto result = new WebResponseDto();
        if(e instanceof BusinessException){
            logger.error("【自定义异常】:{}"+e.getMessage());
            result.setFlag(WebResponseDto.INFO_ERROR);
            result.setMessage(e.getMessage());
        }else{
            logger.error("【系统异常】:{}"+e.getMessage());
            result.setFlag(WebResponseDto.INFO_ERROR);
            result.setMessage("操作失败，请联系管理员");
            e.printStackTrace();
        }
        return result;
    }
}
