package com.sc.common.entity.admin.lookup;

import com.sc.common.dto.PageDto;

/**
 * Created by wust on 2019/4/29.
 */
public class SysLookupSearch extends SysLookup {
    private static final long serialVersionUID = -2630562136316172217L;

    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}
