package com.sc.common.interceptors;

import com.sc.common.interceptors.context.StrategyContext;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by wust on 2019/6/17.
 */
public class ContextHandlerDefaultInterceptor implements HandlerInterceptor {
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) {
        if(o instanceof HandlerMethod){
           String name = ((HandlerMethod) o).getBean().getClass().getName();
            if(name.contains("springfox") || name.contains("swagger")){
                return true;
            }
            StrategyContext.getInstance().setDefaultBusinessContext((HandlerMethod) o);
            return true;
        }

        if(o instanceof ResourceHttpRequestHandler){
            return true;
        }

        return false;
    }
}
