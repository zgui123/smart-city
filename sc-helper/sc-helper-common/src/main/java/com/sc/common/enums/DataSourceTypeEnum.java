package com.sc.common.enums;

public enum DataSourceTypeEnum {
    DRUID("druid","druid数据源"),
    SHARDINGSPHERE("shardingsphere","shardingsphere数据源");

    private String code;

    private String label;

    DataSourceTypeEnum(String code,String label){
        this.code = code;
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
