package com.sc.common.config;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

@SpringBootConfiguration
public class PropertiesAutoConfiguration {

    /**
     * 读取配置，可以在helper-common.jar里面获取外部工程配置
     * @return
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer applicationProperties() {
        ResourcePatternResolver resourceLoader = new PathMatchingResourcePatternResolver();
        Resource resource = resourceLoader.getResource("classpath:/application.yml");
        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();

        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
        yaml.setResources(resource);
        configurer.setProperties(yaml.getObject());
        return configurer;
    }
}
