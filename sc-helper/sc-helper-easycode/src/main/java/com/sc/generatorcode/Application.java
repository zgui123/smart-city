package com.sc.generatorcode;

import com.sc.generatorcode.invoker.SingleInvoker;
import com.sc.generatorcode.invoker.base.Invoker;

/**
 * @author ：wust
 * Date   2018/9/5
 */
public class Application {

    public static void main(String[] args) {
        single();
    }


    public static void single() {
        Invoker invoker = new SingleInvoker.Builder()
                .setTableName("sys_user")
                .setClassName("SysUser")
                .build();
        invoker.execute();
    }

}
