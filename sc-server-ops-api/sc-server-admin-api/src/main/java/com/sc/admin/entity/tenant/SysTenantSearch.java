package com.sc.admin.entity.tenant;

import com.sc.common.dto.PageDto;
import lombok.*;
import io.swagger.annotations.ApiModel;

/**
 * @author: wust
 * @date: 2020-12-03 10:00:18
 * @description:
 */
 @ApiModel(description = "查询对象-SysTenantSearch")
 @NoArgsConstructor
 @AllArgsConstructor
 @ToString
 @Data
public class SysTenantSearch extends SysTenant {
    private PageDto pageDto;
}